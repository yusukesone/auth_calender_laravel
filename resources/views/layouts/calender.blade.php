@section('calender')
@if(substr(url()->full(), -4) === "home")
@include('layouts.calender1910')
@elseif(substr(url()->full(), -4) === "1909")
@include('layouts.calender1909')
@elseif(substr(url()->full(), -4) === "1910")
@include('layouts.calender1910')
@elseif(substr(url()->full(), -4) === "1911")
@include('layouts.calender1911')
@else
<p>TOPへ戻ります。</p>
<script type="text/javascript">
	$(window).load(function(){
		setTimeout(function(){
			window.location.href = "/home";
		},1000);	
	});
</script>
@endif
@endsection
