@extends('layouts.app')


<!DOCTYPE html>
<html lang="ja">
<head>
	<title>MyCalender</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body>
@section('content')
	<div class="container">
        <h1>MyCalender</h1>
        <div class="text"><p>ご希望の日にちを選択すると、予約画面へ進みます。</p></div>
		@include('layouts.calender')
	</div>

	@endsection

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/button.js"></script>
	<script type="text/javascript">
		var book = @json($book);
		var books =[];
		var bookTimes = [];
		var booked = [];
		for (var i = 0 ; i < book.length; i++) {
			books[i] = book[i]["schedule_id"];
			bookTimes[i] = book[i]["book_time"];
		}
		// console.log(books);
		// console.log(bookTimes);
		$.each(books,function(index,value){
			booked[index] = value.substr(-10,4)+value.substr(-5,2)+value.substr(-2);
		});

		$(document).ready(function() {
			$.each(booked,function(index,value){
				var bookmap = booked.filter(function(elem){
					return elem == value ;
				});
				console.log(bookmap);
				if (bookmap.length === 2) {
					$("#"+value).addClass("hidden");
					$("#non"+value).removeClass("hidden");
				}else if(bookmap.length === 1){
					$("#"+value).css("color","red");
					$("#non"+value).addClass("hidden");
				}
			});
		});
	</script>