<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\schedule;
use App\Book;
use App\User;
use Illuminate\Validation\Rule;

class schedulesController extends Controller
{
    //
 //    public function __construct(){
	// 	$this->middleware('auth');
	// }
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        $this->validate($request, [
            'id' => 'required|digits:8'
        ],[
            'id.required' => '',
            'id.digits' => '',
        ]);

        $book = Book::select("book_time")->where("schedule_id" , "=", $request->id)->get();

    	return view('book.index')->with([
            "request" => $request,
            "book" => $book
        ]);

    }

    public function confirm(Request $request){

    	$this->validate($request, [
    		'book_time' => 'required',
            'book_time' => Rule::unique('books','book_time')->where('schedule_id',$request->id),
    		'name' => 'required|max:10',
    		'tel' => 'required|digits:11',
            'id' => 'required|digits:8',
            'id' => Rule::unique('books','schedule_id')
            ->where('user_id', Auth::id())
    	],[
            'book_time.required' => '予約時間の入力は必須です。',
            'book_time.unique' => 'ご希望の予約時間はすでに埋まっています。',
            'name.required' => '名前の入力は必須です。',
            'name.max' => '名前は10文字以内にしてください。',
            'tel.required' => '電話番号の入力は必須です。',
            'tel.digits' => '電話番号は11桁の半角数字で入力してください。ハイフンは不要です。',
            'id.unique' => '同じ日に複数の予約を取ることはできません。',
            'id.required' => '',
            'id.digits' => '',
        ]);

    	return view('book.bookconfirm')->with("request",$request);

    }

    public function store(Request $request){
        $this->validate($request, [
            'book_time' => Rule::unique('books','book_time')->where('schedule_id',$request->id),
            'id' => Rule::unique('books','schedule_id')
            ->where('user_id', Auth::id())
        ],[
            'book_time.unique' => 'ご希望の予約時間はすでに埋まっています。',
            'id.unique' => '同じ日に複数の予約を取ることはできません。'
        ]);
    	$book = new Book();
    	$book->book_time = $request->book_time;
    	$book->name = $request->name;
    	$book->tel = $request->tel;
    	$book->note = $request->note;
    	$book->schedule_id = $request->id;
    	$book->user_id = Auth::id();
    	$book->save();
    	$book = Book::where("schedule_id" , "=", $request->id)->where("user_id" , "=", Auth::id())->value("book_id");

        $request->session()->regenerateToken();

    	return view('book.bookconfirmed')->with([
    		"request" => $request,
    		"book" => $book
    	]);

    }
}
