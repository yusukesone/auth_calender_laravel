<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schedule extends Model
{
    //
	protected $fillable = [
		'id'
	];

	public function books(){
		return $this->hasmany("App\Book");
	}
}
